package com.android.starterkotlin.utilities

import android.graphics.Color
import android.support.design.widget.Snackbar
import android.view.View

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class Utils {
    private var TAG = "Utils"
    private var snackbarMessage: Snackbar? = null

    private object Holder {
        val INSTANCE = Utils()
    }

    companion object {
        val INSTANCE: Utils by lazy { Holder.INSTANCE }
    }

    fun snackBarMessage(rootview: View, message: String) {
        if (!message.isBlank()) {
            snackbarMessage = Snackbar.make(rootview, message, Snackbar.LENGTH_INDEFINITE)
            snackbarMessage!!.setActionTextColor(Color.RED)
            snackbarMessage!!.setAction("OK", {
                snackbarMessage!!.dismiss()
            })
            snackbarMessage!!.show()
        } else {
            snackbarMessage = Snackbar.make(rootview, "null error message", Snackbar.LENGTH_INDEFINITE)
            snackbarMessage!!.setActionTextColor(Color.RED)
            snackbarMessage!!.setAction("OK", {
                snackbarMessage!!.dismiss()
            })
            snackbarMessage!!.show()
        }
    }
}