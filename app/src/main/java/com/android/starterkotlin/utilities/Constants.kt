package com.android.starterkotlin.utilities

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class Constants {
    private object Holder {
        val INSTANCE = Constants()
    }

    companion object {
        val instance: Constants by lazy { Holder.INSTANCE }
    }

    val PrefKey = "SessionStarter"

    class Extras {
        val TAB_POSITION_KEY = "tab_position"
        val TYPE_ACTIVITY_KEY = "type_activity"
        val RES_UNIQUE_KEY = "res_unique"
        val STATUS_KEY = "status"
    }
}