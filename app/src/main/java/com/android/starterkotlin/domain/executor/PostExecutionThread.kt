package com.android.starterkotlin.domain.executor

import rx.Scheduler

/**
* Created by Fauzi Arnami on 3/4/18.
*/

interface PostExecutionThread {
    val scheduler: Scheduler
}