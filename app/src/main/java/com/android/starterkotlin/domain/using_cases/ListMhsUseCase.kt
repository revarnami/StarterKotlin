package com.android.starterkotlin.domain.using_cases

import com.android.starterkotlin.domain.executor.PostExecutionThread
import com.android.starterkotlin.domain.executor.ThreadExecutor
import com.android.starterkotlin.domain.model.MhsAPIModel
import com.android.starterkotlin.domain.repository.LoginRepository
import rx.Observable
import javax.inject.Inject

/*
 Created by Fauzi Arnami on 3/16/18.
*/

class ListMhsUseCase
@Inject constructor(threadExecutor: ThreadExecutor,
                    postExecutionThread: PostExecutionThread,
                    private val loginRepository: LoginRepository
) : UseCase<MhsAPIModel>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(): Observable<MhsAPIModel>? {
        return this.loginRepository.mahasiswa()
    }
}