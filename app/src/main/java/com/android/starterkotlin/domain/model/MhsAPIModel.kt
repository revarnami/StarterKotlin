package com.android.starterkotlin.domain.model

import com.android.starterkotlin.data.entity.MhsEntity
import io.realm.RealmList

/*
 Created by Fauzi Arnami on 3/16/18.
*/

open class MhsAPIModel {
    open var id = 0

    open var list_mahasiswa = RealmList<MhsEntity>()
}