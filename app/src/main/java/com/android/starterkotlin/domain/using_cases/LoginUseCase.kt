package com.android.starterkotlin.domain.using_cases

import com.android.starterkotlin.domain.executor.PostExecutionThread
import com.android.starterkotlin.domain.executor.ThreadExecutor
import com.android.starterkotlin.domain.model.LoginAPIModel
import com.android.starterkotlin.domain.repository.LoginRepository
import rx.Observable
import javax.inject.Inject

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class LoginUseCase
@Inject constructor(threadExecutor: ThreadExecutor,
                    postExecutionThread: PostExecutionThread,
                    private val loginRepository: LoginRepository
) : UseCase<LoginAPIModel>(threadExecutor, postExecutionThread)
{
    var username: String = ""
    var password: String = ""

    override fun buildUseCaseObservable(): Observable<LoginAPIModel>? {
        return this.loginRepository.login(username, password)
    }
}