package com.android.starterkotlin.domain.model

import com.android.starterkotlin.data.entity.AccessTokenEntity
import com.android.starterkotlin.data.entity.MetaEntity

/**
* Created by Fauzi Arnami on 3/4/18.
*/

open class LoginAPIModel {
    open var id = 0
    open var metaEntity = MetaEntity()
    open var accessTokenEntity = AccessTokenEntity()
}