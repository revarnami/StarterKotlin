package com.android.starterkotlin.domain.executor

import java.util.concurrent.Executor

/**
* Created by Fauzi Arnami on 3/4/18.
*/

interface ThreadExecutor : Executor