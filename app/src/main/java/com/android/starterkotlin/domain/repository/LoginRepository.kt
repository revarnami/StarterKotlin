package com.android.starterkotlin.domain.repository

import com.android.starterkotlin.domain.model.LoginAPIModel
import com.android.starterkotlin.domain.model.MhsAPIModel
import rx.Observable

/**
* Created by Fauzi Arnami on 3/4/18.
*/

interface LoginRepository {
    fun login(email: String,
              password: String
    ): Observable<LoginAPIModel>

    fun mahasiswa(): Observable<MhsAPIModel>
}

