package com.android.starterkotlin.presentation.ui.login

import android.util.Log
import com.android.starterkotlin.domain.model.LoginAPIModel
import com.android.starterkotlin.domain.using_cases.LoginUseCase
import com.android.starterkotlin.presentation.internal.di.scope.PerActivity
import rx.lang.kotlin.FunctionSubscriber
import javax.inject.Inject

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@PerActivity
class LoginPresenter
@Inject
constructor(private val loginUseCase: LoginUseCase) 
{
    private val TAG = "LoginPresenter"
    var view: LoginView? = null

    fun onLogin(username: String, password: String) {
        loginUseCase.username = username
        loginUseCase.password = password
        loginUseCase.execute(FunctionSubscriber<LoginAPIModel>()
                .onNext {
                    Log.e(TAG, "onLogin: ${it.metaEntity.status}")
                    if(it.metaEntity.status == 200) {
                        view?.loginSuccess(it.metaEntity.token, it.metaEntity.unit, it.metaEntity.group, it.metaEntity.level)

                    } else {
                        view?.loginFailed()
                    }
                }
                .onError {
                    Log.e(TAG, "onLogin: login failed ${it.message}")
                    view?.loginFailed()
                }
        )
    }
}