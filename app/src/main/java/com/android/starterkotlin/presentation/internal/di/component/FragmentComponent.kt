package com.android.starterkotlin.presentation.internal.di.component

import com.android.starterkotlin.presentation.internal.di.module.FragmentModule
import com.android.starterkotlin.presentation.internal.di.scope.PerFragment
import dagger.Component

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@PerFragment
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(FragmentModule::class))
interface FragmentComponent {
//    fun inject(topicMainFragment: TopicMainFragment)
}