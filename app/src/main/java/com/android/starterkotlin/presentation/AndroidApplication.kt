package com.android.starterkotlin.presentation

import android.app.Application
import com.android.starterkotlin.presentation.internal.di.component.ApplicationComponent
import com.android.starterkotlin.presentation.internal.di.component.DaggerApplicationComponent
import com.android.starterkotlin.presentation.internal.di.module.ApplicationModule
import io.realm.Realm

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class AndroidApplication : Application() {
    private val TAG = "AndroidApplication"

    val component: ApplicationComponent
        get() = DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()

    lateinit var realm: Realm

    override fun onCreate() {
        super.onCreate()
        initRealm()
        component.inject(this)
    }

    private fun initRealm() {
        Realm.init(this)
    }
}