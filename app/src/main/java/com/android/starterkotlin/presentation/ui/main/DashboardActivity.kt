package com.android.starterkotlin.presentation.ui.main

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.android.starterkotlin.R
import com.android.starterkotlin.data.entity.MhsEntity
import com.android.starterkotlin.presentation.AndroidApplication
import com.android.starterkotlin.presentation.internal.di.component.ActivityComponent
import com.android.starterkotlin.presentation.internal.di.component.DaggerActivityComponent
import com.android.starterkotlin.presentation.internal.di.module.ActivityModule
import io.realm.RealmList
import kotlinx.android.synthetic.main.activity_dashboard.*
import javax.inject.Inject

class DashboardActivity : AppCompatActivity(), DashboardView {

    @Inject
    lateinit var presenter: DashboardPresenter

    private val component: ActivityComponent
        get() = DaggerActivityComponent.builder()
                .applicationComponent((application as AndroidApplication).component)
                .activityModule(ActivityModule(this))
                .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dashboard)
        component.inject(this)
        presenter.view = this
        presenter.showMhsList()
    }

    override fun showListMahasiswa(listMhs: RealmList<MhsEntity>) {
        listMhsRV.setHasFixedSize(true)
        val llm = LinearLayoutManager(this)
        listMhsRV.layoutManager = llm
        val adapter = DashboardRVAdapter(listMhs, this)
        listMhsRV.adapter = adapter
    }
}
