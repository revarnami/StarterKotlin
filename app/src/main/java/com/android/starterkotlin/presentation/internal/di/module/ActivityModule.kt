package com.android.starterkotlin.presentation.internal.di.module

import android.app.Activity
import com.android.starterkotlin.presentation.internal.di.scope.PerActivity
import dagger.Module
import dagger.Provides

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Module
class ActivityModule(private val baseActivity: Activity) {
    /**
     * Expose the activity to dependents in the graph.
     */
    @Provides
    @PerActivity
    internal fun provideActivity(): Activity {
        return this.baseActivity
    }
}