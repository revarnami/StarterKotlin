package com.android.starterkotlin.presentation.ui.main

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.android.starterkotlin.R
import com.android.starterkotlin.data.entity.MhsEntity
import io.realm.RealmList
import kotlinx.android.synthetic.main.row_mahasiwa.view.*

/*
 Created by Fauzi Arnami on 3/16/18.
*/


class DashboardRVAdapter(private val listMHs: RealmList<MhsEntity>,
                         private val activity: Activity
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var layoutInflater: LayoutInflater

    init {
        layoutInflater = activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
    }


    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): RecyclerView.ViewHolder {
        val view = layoutInflater.inflate(R.layout.row_mahasiwa, parent, false)
        val mvh = MhsViewHolder(view)
        return mvh
    }

    override fun getItemCount(): Int {
        return listMHs.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {
        val view = holder!!.itemView
        view.namaMhsTV.text = listMHs[position].nama
    }

    class MhsViewHolder internal constructor(itemView: View) : RecyclerView.ViewHolder(itemView)
}