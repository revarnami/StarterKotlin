package com.android.starterkotlin.presentation.ui.main

import com.android.starterkotlin.data.entity.MhsEntity
import io.realm.RealmList

/*
 Created by Fauzi Arnami on 3/16/18.
*/

interface DashboardView {
    fun showListMahasiswa(listMhs: RealmList<MhsEntity>)
}