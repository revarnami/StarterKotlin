package com.android.starterkotlin.presentation.ui.main

import android.util.Log
import com.android.starterkotlin.domain.model.MhsAPIModel
import com.android.starterkotlin.domain.using_cases.ListMhsUseCase
import com.android.starterkotlin.presentation.internal.di.scope.PerActivity
import rx.lang.kotlin.FunctionSubscriber
import javax.inject.Inject

/*
 Created by Fauzi Arnami on 3/16/18.
*/

@PerActivity
class DashboardPresenter
@Inject
constructor(private val listMhsUseCase: ListMhsUseCase) {
    private val TAG = "DashboardPresenter"
    var view: DashboardView? = null

    fun showMhsList() {
        listMhsUseCase.execute(FunctionSubscriber<MhsAPIModel>()
                .onNext {
                    view?.showListMahasiswa(it.list_mahasiswa)
                }
                .onError {
                    Log.e(TAG, "onLogin: login failed ${it.message}")
                }
        )
    }
}