package com.android.starterkotlin.presentation.internal.di.component

import com.android.starterkotlin.presentation.internal.di.module.ActivityModule
import com.android.starterkotlin.presentation.internal.di.scope.PerActivity
import com.android.starterkotlin.presentation.ui.login.MainActivity
import com.android.starterkotlin.presentation.ui.main.DashboardActivity
import dagger.Component

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@PerActivity
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(ActivityModule::class))
interface ActivityComponent {
    fun inject(loginActivity: MainActivity)
    fun inject(dashboardActivity: DashboardActivity)

}