package com.android.starterkotlin.presentation.internal.di.component

import android.content.Context
import android.content.SharedPreferences
import com.android.starterkotlin.domain.executor.PostExecutionThread
import com.android.starterkotlin.domain.executor.ThreadExecutor
import com.android.starterkotlin.domain.repository.LoginRepository
import com.android.starterkotlin.presentation.AndroidApplication
import com.android.starterkotlin.presentation.internal.di.module.ApplicationModule
import com.android.starterkotlin.presentation.navigation.Navigator
import dagger.Component
import javax.inject.Singleton

/**
* Created by Fauzi Arnami on 3/4/18.
*/


@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = arrayOf(ApplicationModule::class))
interface ApplicationComponent {

    fun inject(androidApplication: AndroidApplication)

    val androidApplication: AndroidApplication

    fun context(): Context

    fun sharedPreferences(): SharedPreferences

    fun sharedPreferencesEditor(): SharedPreferences.Editor

    fun navigator(): Navigator

    fun threadExecutor(): ThreadExecutor

    fun postExecutionThread(): PostExecutionThread

    fun loginRepository(): LoginRepository

}
