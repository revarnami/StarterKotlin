package com.android.starterkotlin.presentation.ui.login

import android.content.Context

/**
* Created by Fauzi Arnami on 3/4/18.
*/

interface LoginView {
    val contextView: Context
    fun loginSuccess(nama: String, unit: String, group: String, level: Int)
    fun loginFailed()
}