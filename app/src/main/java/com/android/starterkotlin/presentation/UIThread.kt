package com.android.starterkotlin.presentation

import com.android.starterkotlin.domain.executor.PostExecutionThread
import rx.Scheduler
import javax.inject.Inject
import javax.inject.Singleton
import rx.android.schedulers.AndroidSchedulers

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Singleton
class UIThread
@Inject
constructor() : PostExecutionThread {

    override val scheduler: Scheduler
        get() = AndroidSchedulers.mainThread()
}