package com.android.starterkotlin.presentation.navigation

import android.app.Activity
import android.content.Context
import android.content.Intent
import com.android.starterkotlin.R.anim.left_in
import com.android.starterkotlin.R.anim.right_out
import com.android.starterkotlin.presentation.ui.login.MainActivity
import com.android.starterkotlin.presentation.ui.main.DashboardActivity
import javax.inject.Inject
import javax.inject.Singleton

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Singleton
class Navigator
@Inject
constructor()

/**
 * Goes to the user list screen.

 * @param context A Context needed to open the destiny activity.
 */

fun navigateToMainPage(context: Context?) {
    if (context != null && context is Activity) {
        val activity = context
        val flags = context.flags(Intent.FLAG_ACTIVITY_NEW_TASK, Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.start<MainActivity>(flags, left_in, right_out)
    }
}

fun navigateToDashboardPage(context: Context?) {
    if (context != null && context is Activity) {
        val activity = context
        val flags = context.flags(Intent.FLAG_ACTIVITY_NEW_TASK, Intent.FLAG_ACTIVITY_CLEAR_TOP)
        activity.start<DashboardActivity>(flags, left_in, right_out)
    }
}