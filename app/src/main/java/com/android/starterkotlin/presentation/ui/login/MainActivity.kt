package com.android.starterkotlin.presentation.ui.login

import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.android.starterkotlin.R
import com.android.starterkotlin.presentation.AndroidApplication
import com.android.starterkotlin.presentation.internal.di.component.ActivityComponent
import com.android.starterkotlin.presentation.internal.di.component.DaggerActivityComponent
import com.android.starterkotlin.presentation.internal.di.module.ActivityModule
import com.android.starterkotlin.presentation.navigation.navigateToDashboardPage
import com.android.starterkotlin.utilities.Utils
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(),LoginView {
    private val TAG = "MainActivity"
    @Inject
    lateinit var presenter: LoginPresenter

    override val contextView: Context
        get() = this

    private val component: ActivityComponent
        get() = DaggerActivityComponent.builder()
                .applicationComponent((application as AndroidApplication).component)
                .activityModule(ActivityModule(this))
                .build()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        component.inject(this)
        presenter.view = this
        authenticate_btn.setOnClickListener {
            val email = email_login_et.editText?.text.toString()
            val password = password_login_et.editText?.text.toString()
            Log.e(TAG, "onCreate: email = $email")
            Log.e(TAG, "onCreate: password = $password")
            presenter.onLogin(email, password)
        }

    }

    override fun loginSuccess(token: String, unit: String, group: String, level: Int) {

        Utils().snackBarMessage(rootview_login, "Login berhasil token ${token}")
        navigateToDashboardPage(this)
//        namaTV.text = token
    }

    override fun loginFailed() {
        Utils().snackBarMessage(rootview_login, "Login gagal")
    }
}
