package com.android.starterkotlin.presentation.internal.di.module

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import com.android.starterkotlin.data.executor.JobExecutor
import com.android.starterkotlin.data.repository.LoginDataRepository
import com.android.starterkotlin.domain.executor.PostExecutionThread
import com.android.starterkotlin.domain.executor.ThreadExecutor
import com.android.starterkotlin.domain.repository.LoginRepository
import com.android.starterkotlin.presentation.AndroidApplication
import com.android.starterkotlin.presentation.UIThread
import com.android.starterkotlin.utilities.Constants
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Module
class ApplicationModule(private val androidApplication: AndroidApplication) {
    @Provides
    @Singleton
    fun application(): AndroidApplication = androidApplication

    @Provides
    @Singleton
    fun provideApplicationContext(): Context = androidApplication

    @SuppressLint("WrongConstant")
    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences = androidApplication.getSharedPreferences(Constants().PrefKey, Context.MODE_APPEND)

    @SuppressLint("WrongConstant", "CommitPrefEdits")
    @Provides
    @Singleton
    fun provideSharedPreferencesEditor(): SharedPreferences.Editor = androidApplication.getSharedPreferences(Constants().PrefKey, Context.MODE_APPEND).edit()

    @Provides
    @Singleton
    fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor = jobExecutor

    @Provides
    @Singleton
    fun providePostExecutionThread(uiThread: UIThread): PostExecutionThread = uiThread

    @Provides
    @Singleton
    fun provideLoginRepository(dataRepository: LoginDataRepository): LoginRepository = dataRepository
}