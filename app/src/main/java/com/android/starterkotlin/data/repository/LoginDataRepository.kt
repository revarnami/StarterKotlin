package com.android.starterkotlin.data.repository

import com.android.starterkotlin.data.entity.mapper.LoginMapper
import com.android.starterkotlin.data.entity.mapper.MhsMapper
import com.android.starterkotlin.data.repository.datasources.login.LoginDataFactory
import com.android.starterkotlin.domain.model.LoginAPIModel
import com.android.starterkotlin.domain.model.MhsAPIModel
import com.android.starterkotlin.domain.repository.LoginRepository
import rx.Observable
import javax.inject.Inject

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class LoginDataRepository
@Inject constructor(private val mapper: LoginMapper,
                    private val mhsMapper: MhsMapper,
                    private val dataFactory: LoginDataFactory
) : LoginRepository
{
    override fun login(email: String, password: String): Observable<LoginAPIModel> {
        return dataFactory.createCloudDataStore()
                .login(email, password)
                .map({ mapper.transform(it) })
    }

    override fun mahasiswa(): Observable<MhsAPIModel> {
        return dataFactory.createCloudDataStore()
                .mahasiswa()
                .map({ mhsMapper.transform(it) })
    }
}