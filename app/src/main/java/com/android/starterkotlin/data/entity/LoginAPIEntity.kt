package com.android.starterkotlin.data.entity

import com.google.gson.annotations.Expose
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@RealmClass
open class LoginAPIEntity : RealmObject() {
    @PrimaryKey
    @Expose
    open var id = 1

    @Expose
    open var access_token = AccessTokenEntity()

    @Expose
    open var meta = MetaEntity()
}