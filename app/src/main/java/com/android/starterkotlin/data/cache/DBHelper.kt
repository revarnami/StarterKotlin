package com.android.starterkotlin.data.cache

import com.android.starterkotlin.data.entity.LoginAPIEntity
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmQuery

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class DBHelper {

    private object Holder {
        val INSTANCE = DBHelper()
    }

    companion object {
        val INSTANCE: DBHelper by lazy { Holder.INSTANCE }
    }

    var realm: Realm

    init {
        val config = RealmConfiguration.Builder()
                .name("Starter.realm")
                .schemaVersion(2)
                .deleteRealmIfMigrationNeeded()
                .build()

        realm = Realm.getInstance(config)
    }

    fun saveLogin(login: LoginAPIEntity) {
        realm.beginTransaction()
        realm.copyToRealmOrUpdate(login)
        realm.commitTransaction()
    }

}