package com.android.starterkotlin.data.net

import com.android.starterkotlin.BuildConfig
import com.android.starterkotlin.data.entity.LoginAPIEntity
import com.android.starterkotlin.data.entity.MhsAPIEntity
import com.google.gson.Gson
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Observable
import javax.inject.Inject
import javax.inject.Singleton


/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Singleton
class RestAPI
@Inject
constructor() {
    private var service: StarterService

    init {
        val builder = OkHttpClient.Builder().addInterceptor(getLogginInterceptor())
        val retro = Retrofit.Builder().baseUrl(BuildConfig.SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(builder.build()).build()
        service = retro.create(StarterService::class.java)
    }

    fun getLogginInterceptor(): Interceptor {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        return interceptor
    }

    fun postLogin(email: String,
                  password: String
    ): Observable<LoginAPIEntity> = service.postLogin(email, password)

    fun postMahasiswa(): Observable<MhsAPIEntity> = service.postMahasiswa()
}
