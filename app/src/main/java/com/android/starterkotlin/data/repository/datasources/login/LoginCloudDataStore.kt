package com.android.starterkotlin.data.repository.datasources.login

import com.android.starterkotlin.data.cache.DBHelper
import com.android.starterkotlin.data.entity.LoginAPIEntity
import com.android.starterkotlin.data.entity.MhsAPIEntity
import com.android.starterkotlin.data.net.RestAPI
import rx.Observable

/**
* Created by Fauzi Arnami on 3/4/18.
*/

class LoginCloudDataStore(private val restAPI: RestAPI) : LoginDataStore
{
    private val TAG = "LoginCloudDataStore"

    override fun login(email: String, password: String): Observable<LoginAPIEntity> {
        return restAPI.postLogin(email, password)
                .doOnNext {
                    DBHelper().saveLogin(it)
                }
    }

    override fun mahasiswa(): Observable<MhsAPIEntity> {
        return restAPI.postMahasiswa()
                .doOnNext {
                    //                    DBHelper().saveLogin(it)
                }
    }
}