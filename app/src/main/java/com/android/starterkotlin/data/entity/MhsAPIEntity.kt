package com.android.starterkotlin.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/*
 Created by Fauzi Arnami on 3/16/18.
*/

@RealmClass
open class MhsAPIEntity : RealmObject() {
    @PrimaryKey
    @Expose
    open var id = 0

    @Expose
    @SerializedName("list_mahasiswa")
    open var list_mahasiswa = RealmList<MhsEntity>()
}