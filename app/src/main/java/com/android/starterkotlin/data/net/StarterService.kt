package com.android.starterkotlin.data.net

import com.android.starterkotlin.data.entity.LoginAPIEntity
import com.android.starterkotlin.data.entity.MhsAPIEntity
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST
import rx.Observable

/**
* Created by Fauzi Arnami on 3/4/18.
*/

interface StarterService {
    //POST Section
    @FormUrlEncoded
    @POST("API_mahasiswa/login")
    fun postLogin(
            @Field("username") email: String,
            @Field("password") password: String
    ): Observable<LoginAPIEntity>

    @POST("API_mahasiswa/mahasiswa")
    fun postMahasiswa(): Observable<MhsAPIEntity>
}