package com.android.starterkotlin.data.entity.mapper

import com.android.starterkotlin.data.entity.LoginAPIEntity
import com.android.starterkotlin.domain.model.LoginAPIModel
import javax.inject.Inject
import javax.inject.Singleton

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Singleton
class LoginMapper
@Inject
constructor() : EntryEntityMapper<LoginAPIModel, LoginAPIEntity>()
{
    override fun transform(entity: LoginAPIEntity?): LoginAPIModel? {
        if (entity != null) {
            val loginModel = LoginAPIModel()
            loginModel.id = entity.id
            loginModel.accessTokenEntity = entity.access_token
            loginModel.metaEntity = entity.meta
            return loginModel
        }
        return null
    }
}