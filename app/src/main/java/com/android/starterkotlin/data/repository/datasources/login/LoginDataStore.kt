package com.android.starterkotlin.data.repository.datasources.login

import com.android.starterkotlin.data.entity.LoginAPIEntity
import com.android.starterkotlin.data.entity.MhsAPIEntity
import rx.Observable

/**
* Created by Fauzi Arnami on 3/4/18.
*/

interface LoginDataStore {
    fun login(email: String,
              password: String
    ): Observable<LoginAPIEntity>


    fun mahasiswa(): Observable<MhsAPIEntity>

}