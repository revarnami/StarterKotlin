package com.android.starterkotlin.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@RealmClass
open class AccessTokenEntity : RealmObject() {
    @PrimaryKey
    @Expose
    open var id = 1

    @Expose
    open var token = ""

    @Expose
    @SerializedName("refresh_token")
    open var refreshToken = ""
}