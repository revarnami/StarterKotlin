package com.android.starterkotlin.data.entity.mapper

import com.android.starterkotlin.data.entity.MhsAPIEntity
import com.android.starterkotlin.domain.model.MhsAPIModel
import javax.inject.Inject
import javax.inject.Singleton

/*
 Created by Fauzi Arnami on 3/16/18.
*/

@Singleton
class MhsMapper
@Inject
constructor() : EntryEntityMapper<MhsAPIModel, MhsAPIEntity>() {
    override fun transform(entity: MhsAPIEntity?): MhsAPIModel? {
        if (entity != null) {
            val mhsModel = MhsAPIModel()
            mhsModel.id = entity.id
            mhsModel.list_mahasiswa = entity.list_mahasiswa
            return mhsModel
        }
        return null
    }
}