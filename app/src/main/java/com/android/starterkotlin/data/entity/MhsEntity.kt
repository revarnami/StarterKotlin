package com.android.starterkotlin.data.entity

import com.google.gson.annotations.Expose
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/*
 Created by Fauzi Arnami on 3/16/18.
*/

@RealmClass
open class MhsEntity : RealmObject() {
    @PrimaryKey
    @Expose
    open var id = 0

    @Expose
    open var id_mhs = ""

    @Expose
    open var nim = ""


    @Expose
    open var nama = ""

    @Expose
    open var telp = ""

    @Expose
    open var alamat = ""

    @Expose
    open var unit = 0
}