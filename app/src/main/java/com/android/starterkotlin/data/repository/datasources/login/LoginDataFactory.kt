package com.android.starterkotlin.data.repository.datasources.login

import com.android.starterkotlin.data.net.RestAPI
import javax.inject.Inject
import javax.inject.Singleton

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@Singleton
class LoginDataFactory
@Inject
constructor(private val restAPI: RestAPI) {
    fun createCloudDataStore(): LoginDataStore {
        return LoginCloudDataStore(restAPI)
    }
}