package com.android.starterkotlin.data.entity

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import io.realm.annotations.RealmClass

/**
* Created by Fauzi Arnami on 3/4/18.
*/

@RealmClass
open class MetaEntity : RealmObject() {
    @PrimaryKey
    @Expose
    open var id = 1

    @Expose
    open var status = 0

    @Expose
    @SerializedName("error_message")
    open var errorMessage = ""

    @Expose
    open var message = ""

    @Expose
    @SerializedName("error_type")
    open var errorType = ""

    @Expose
    open var errors = RealmList<ErrorEntity>()

    @Expose
    open var nama = ""

    @Expose
    open var unit = ""

    @Expose
    open var group = ""

    @Expose
    open var level = 0

    @Expose
    open var token = ""
}